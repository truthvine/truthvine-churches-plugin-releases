<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.truthvine.com
 * @since      1.0.0
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/public
 * @author     TruthVine CIC <team@truthvine.com>
 */
class Truthvine_Churches_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Truthvine_Churches_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Truthvine_Churches_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/truthvine-churches-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Truthvine_Churches_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Truthvine_Churches_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/truthvine-churches-public.js', array( 'jquery' ), $this->version, false );

	}

	public function get_attachement_url_for_private_files($url, $postId) {
		// This filter is called when WP is generating the url for an attachement
		
		// If the file is private, then we redirect access to the file via our private_file_download handler
        if (metadata_exists('post', $postId, 'tv_churches_restricted_roles'))
        {
			$file = get_post_meta( $postId, '_wp_attached_file', true );
            $url = admin_url("admin-post.php?action=private_file_download&file=" . $file);
        }

        return $url;
    }

	public function handle_private_file_download_when_not_logged_in() {
		auth_redirect();
		exit;
	}

	public function handle_private_file_download_when_logged_in() {
		$attachementId = attachment_url_to_postid($_REQUEST["file"]); 

		if ($attachementId == 0) {
			// TODO: proper error page
			status_header(404);
			wp_die ("The file you requested could not be found.");
			return;
		}

		$roleRestrictions = get_post_meta( $attachementId, 'tv_churches_restricted_roles', true);
		if (count($roleRestrictions) === 0) {
			status_header(403);
			wp_die ("You are not permitted to download this file.");
			return;
		}

		$user = wp_get_current_user();
		if ($this->do_role_lists_intersect($user->roles, $roleRestrictions)) {
			// the user is in at least one of the roles permitted to download the file
			// so transmit the file to them

			$filePath = get_attached_file($attachementId);
			$fileName = basename($filePath);

			// see https://github.com/WordPress/WordPress/blob/master/wp-includes/ms-files.php for how Wordpress handles downloads
			// when it doesn't hand off to the host server

			// determine the mime type of the file
			$mime = wp_check_filetype( $filePath );
			if ( $mime['type'] ) {
				$mimetype = $mime['type'];
			} else {
				$mimetype = 'image/' . substr( $filePath, strrpos( $filePath, '.' ) + 1 );
			}

			header("Expires: 0");
  			header("Cache-Control: no-cache, no-store, must-revalidate"); 
  			header('Cache-Control: pre-check=0, post-check=0, max-age=0', false); 
  			header("Pragma: no-cache");	

			header( 'Content-Type: ' . $mimetype );
			header("Content-Disposition:inline; filename={$fileName}");
    		header('Content-Length: ' . filesize($filePath));

			// write the file to the output stream
			readfile($filePath);
			flush();
			
			exit;
		}
		else {
			status_header(403);
			wp_die ("You are not permitted to download this file.");
		}

	}

	public function filter_nav_menus(array $sorted_menu_items) {
		// $user = wp_get_current_user();

		// foreach ($sorted_menu_items as $key => $item) {
		// 	if ($item instanceof WP_Post) {
		// 		$hideIfNotAuthorized = get_post_meta( $item->ID, 'tv_churches_hide_in_nav', false);
		// 		if (!$hideIfNotAuthorized) {
		// 			continue;
		// 		}

		// 		$roleRestrictions = get_post_meta( $item->ID, 'tv_churches_restricted_roles', false);
		// 		if (is_array($roleRestrictions) && count($roleRestrictions) > 0) {
		// 			if (!$user || !$this->do_role_lists_intersect($user->roles, $roleRestrictions)) {
		// 				// the post has restrictions, but the user is not logged in, so it must be hidden
		// 				unset($sorted_menu_items[$key]);
		// 			}
		// 		}
		// 	}
		// }

		// $sorted_menu_items = array_values($sorted_menu_items);
		
		return $sorted_menu_items;
	}

	public function check_page_authorization($original_template) {
		global $post;
		
		$roleRestrictions = get_post_meta( $post->ID, 'tv_churches_restricted_roles', false);
		if (is_array($roleRestrictions) && count($roleRestrictions) > 0) {
			
			// we now know that there are some role restrictions, so we must require the user to have signed in
			if (!is_user_logged_in()) {
				auth_redirect();
			}

			$user = wp_get_current_user();
			if (!$this->do_role_lists_intersect($user->roles, $roleRestrictions)) {
				// the user is not in any of the roles required to view this page so 
				// change to a template that hides the page
				return plugin_dir_path( dirname( __FILE__ ) )."includes/restricted-page.php";
			}
		}
			
		// there are no restrictions, or the user passed the necessary tests, so render the page
		return $original_template;
	}

	private function do_role_lists_intersect(array $needle, array $haystack) {
		foreach ($needle as $role) {
			if (in_array($role, $haystack)) {
				return true;
			}
		}

		return false;
	}

	

}
