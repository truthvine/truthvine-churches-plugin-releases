<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.truthvine.com
 * @since      1.0.0
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
