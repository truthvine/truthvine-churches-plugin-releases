<?php
require_once ABSPATH . 'wp-admin/includes/user.php';

class Truthvine_Churches_API
{

    public function register_routes()
    {
        register_rest_route('truthvine-churches/v1', '/wp-roles', array(
            'methods' => 'GET',
            'callback' => array($this, 'get_roles')
        ));
    }

    public function get_roles(WP_REST_Request $request)
    {
        $roles = get_editable_roles();

        $role_names = array_map(
                function ($key, $item) {
                    return array( "id" => $key, "name" => $item["name"]);
                },
                array_keys($roles), $roles
            );

        return new WP_REST_Response($role_names);
    }
}
