<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.truthvine.com
 * @since      1.0.0
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/includes
 * @author     TruthVine CIC <team@truthvine.com>
 */
class Truthvine_Churches {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Truthvine_Churches_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'TRUTHVINE_CHURCHES_VERSION' ) ) {
			$this->version = TRUTHVINE_CHURCHES_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'truthvine-churches';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_api_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Truthvine_Churches_Loader. Orchestrates the hooks of the plugin.
	 * - Truthvine_Churches_i18n. Defines internationalization functionality.
	 * - Truthvine_Churches_Admin. Defines all hooks for the admin area.
	 * - Truthvine_Churches_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-truthvine-churches-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-truthvine-churches-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-truthvine-churches-admin.php';

		/**
		 * The class responsible for defining actions relating the Page Restrictions feature.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-truthvine-churches-page-restrictions-admin.php';

		/**
		 * The class responsible for defining actions relating to the Plugin's API.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'api/class-truthvine-churches-api.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-truthvine-churches-public.php';

		
		$this->loader = new Truthvine_Churches_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Truthvine_Churches_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Truthvine_Churches_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Truthvine_Churches_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_admin, 'register_scripts' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'enqueue_block_editor_assets', $plugin_admin, 'enqueue_block_editor_scripts' );
		
		$plugin_page_restrictions = new Truthvine_Churches_Page_Restrictions_Admin();
		$plugin_page_restrictions->register($this->loader);
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Truthvine_Churches_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		// This handler intercepts the rendering of pages to check if the user is authorised to view them
		// If not, it renders an "Access forbidden" in its place
		$this->loader->add_filter('template_include', $plugin_public, 'check_page_authorization');

		// The following two handlers are used to handle attempts to download files from the "private" uploads folder
		$this->loader->add_action ( 'admin_post_private_file_download', $plugin_public, 'handle_private_file_download_when_logged_in');
		$this->loader->add_action ( 'admin_post_nopriv_private_file_download', $plugin_public, 'handle_private_file_download_when_not_logged_in');

		// This filter is called when WP is generating attachement urls, and rewrites the url for private files to go via our handler
		$this->loader->add_filter('wp_get_attachment_url', $plugin_public, 'get_attachement_url_for_private_files', 10, 2);

		//$this->loader->add_filter( 'wp_nav_menu_objects', $plugin_public, 'filter_nav_menus');
	}

	private function define_api_hooks() {
		$api = new Truthvine_Churches_API();

		$this->loader->add_action('rest_api_init', $api, 'register_routes');
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Truthvine_Churches_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
