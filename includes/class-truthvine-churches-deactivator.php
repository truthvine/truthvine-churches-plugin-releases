<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.truthvine.com
 * @since      1.0.0
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/includes
 * @author     TruthVine CIC <team@truthvine.com>
 */
class Truthvine_Churches_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
