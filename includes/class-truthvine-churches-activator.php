<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.truthvine.com
 * @since      1.0.0
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/includes
 * @author     TruthVine CIC <team@truthvine.com>
 */
class Truthvine_Churches_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		// put a web.config (on Windows) or ".htaccess" file (on Linux) at the root of the private files upload folder
		$fileName = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? "web.config" : ".htaccess";
		$sourcePath = TRUTHVINE_CHURCHES_DIR . "assets/" . $fileName;

		$privateFolderPath = wp_upload_dir(false, true)["basedir"]."/private";

		$destPath = $privateFolderPath."/".$fileName;

		if (!file_exists($privateFolderPath)) {
			mkdir($privateFolderPath);
		}

		copy($sourcePath, $destPath);
	}

}
