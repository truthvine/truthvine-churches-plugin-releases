<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.truthvine.com
 * @since             0.9.4
 * @package           Truthvine_Churches
 *
 * @wordpress-plugin
 * Plugin Name:       TruthVine Churches
 * Plugin URI:        http://www.truthvine.com
 * Description:       Provides functionality that enhances Church websites, like Members-only areas
 * Version:           1.0.5
 * Author:            TruthVine CIC
 * Author URI:        http://www.truthvine.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       truthvine-churches
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'TRUTHVINE_CHURCHES_VERSION', '1.0.5' );
define( 'TRUTHVINE_CHURCHES_DIR', plugin_dir_path(__FILE__));

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-truthvine-churches-activator.php
 */
function activate_truthvine_churches() {
	require_once TRUTHVINE_CHURCHES_DIR. 'includes/class-truthvine-churches-activator.php';
	Truthvine_Churches_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-truthvine-churches-deactivator.php
 */
function deactivate_truthvine_churches() {
	require_once TRUTHVINE_CHURCHES_DIR . 'includes/class-truthvine-churches-deactivator.php';
	Truthvine_Churches_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_truthvine_churches' );
register_deactivation_hook( __FILE__, 'deactivate_truthvine_churches' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require TRUTHVINE_CHURCHES_DIR. 'includes/class-truthvine-churches.php';

require_once TRUTHVINE_CHURCHES_DIR . 'includes/class-truthvine-churches-loader.php';

require_once TRUTHVINE_CHURCHES_DIR . 'includes/plugin-update-checker-4.7/plugin-update-checker.php';

// check for new versions of the plugin
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/truthvine/truthvine-churches-plugin-releases',
	__FILE__, //Full path to the main plugin file or functions.php.
	'truthvine-churches'
);

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_truthvine_churches() {

	$plugin = new Truthvine_Churches();
	$plugin->run();

}
run_truthvine_churches();
