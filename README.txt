=== TruthVine Church Websites Plugin ===
Contributors: TruthVine
Donate link: http://www.truthvine.com
Tags: membership
Requires PHP: 7.0
Requires at least: 5.0
Tested up to: 5.4.0
Stable tag: 1.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enhances Wordpress with features like private membership pages.

== Description ==

Enhances Wordpress with features like private membership pages.

== Changelog ==
= 1.0.5 =
* Further bug fix for uploading private files through Media Upload popup

= 1.0.4 =
* Fixed bug where browser continued to load old versions of admin scripts because of caching

= 1.0.3 =
* Fixed bug where suprious text was displayed in background of Admin area.

= 1.0.2 =
* Fixed bug where reusable blocks editor would not display because of an error.

= 1.0.1 =
* Fixed bug where downloaded private files were not given the correct filename.

= 1.0 =
* The initial version
