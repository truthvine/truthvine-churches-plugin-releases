<?php

/**
 * Adds ability to restrict pages by Role
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/page-restrictions
 * @author     TruthVine CIC <team@truthvine.com>
 */
class Truthvine_Churches_Page_Restrictions_Admin
{

    /**
     * @param Truthvine_Churches_Loader $loader
     */
    public function register(Truthvine_Churches_Loader $loader)
    {
        register_post_meta('', 'tv_churches_restricted_roles', array(
            'show_in_rest' => true,
            'single' => false,
            'type' => 'string'
        ));
        register_post_meta('', 'tv_churches_hide_in_nav', array(
            'show_in_rest' => true,
            'single' => true,
            'type' => 'boolean'
        ));

        $loader->add_action('admin_init', $this, 'add_admin_actions');
    }

    public function add_admin_actions()
    {
        // The following action is called in the process of rendering the upload ui
        // We intercept it to add a "Make private" checkbox and to add a script for handling that checkbox
        add_action("pre-plupload-upload-ui", array($this, 'customise_uploader'));

        // the following filter is called when Wordpress is handling a file upload
        // We intercept it to change the upload directory to our "private" folder
        add_filter('wp_handle_upload_prefilter', array($this, 'check_for_private_file_upload'));

        // the following action is called by wp_insert_post once it has added an attachement to the database
        // it will be used the very first time a private file is uploaded to set the tv_churches_restricted_roles metadata field
        add_filter('add_attachment', array($this, 'update_metadata_for_private_attachement'));

        add_action('edit_attachment', array($this, 'save_attachement_meta_data'));

        add_meta_box(
            'tvpr-attachement-roles-meta-box',
            'Access Security',
            array($this, 'render_attachement_roles_meta_box'),
            'attachment',
            'side',
            'default'
        );

        
    }

    public function customise_uploader()
    {
        // The value of this checkbox is read by the script in uploader-extensions.ts and POSTed with the upload request
        // The <img src onerror=...> element is a hack to enable the UploaderExtensions.initialise function to be called
        // We need this here because the static initialisation used in uploader-extensions.ts kicks in too early on the media-new.php page
        ?>
        <div class=" tvpr-make-private-label">
            <label>
                <input id="tvpr_uploader_make_private" type="checkbox"> Make file private
            </label>
            <img src onerror="truthVine.UploaderExtensions.initialise()">
        </div>
        <?php
    }

    public function save_attachement_meta_data()
    {
        // This function is called when an attached is saved
        // It takes the list of roles which the user has selected and saves it to the tv_churches_restricted_roles metadata field
        global $post;

        if (metadata_exists('post', $post->ID, 'tv_churches_restricted_roles'))
        {
            if (isset($_POST['tvpr-roles'])) {
                update_post_meta($post->ID, 'tv_churches_restricted_roles', $_POST['tvpr-roles']);
            }
            else {
                update_post_meta($post->ID, 'tv_churches_restricted_roles', array());
            }
        }
    }

    public function render_attachement_roles_meta_box()
    {
        global $post;

        if (metadata_exists('post', $post->ID, 'tv_churches_restricted_roles')) {
            $roleRestrictions = get_post_meta($post->ID, 'tv_churches_restricted_roles', true);


            $roles = wp_roles();

            $select = "<ul>";
            foreach ($roles->role_names as $key => $roleName) {
                // note: by specifying the input name with a [] suffix, we ensure that PHP will return an array of the checkboxes which have been checked
                $select .= "\n\t<li><label class='selectit'><input value='" . esc_attr($key) . "' type='checkbox' name='tvpr-roles[]' " . $this->checked_if_in_array($roleRestrictions, $key) . " />" . $roleName . "</label></li>";
            }

            $select .= "</ul>";

            echo "<p>Which Roles which can access this file?</p>";
            echo $select;
        } else {
            echo "<p>Upload as a Private file to control access security.</p>";
        }
    }

    private function checked_if_in_array(array $array, $value)
    {
        if (in_array($value, $array)) {
            return "checked";
        }

        return "";
    }

    public function check_for_private_file_upload($file)
    {
        // This filter is called when Wordpress is uploading a new file
        // We look for the trpr_make_private parameter that our script in the browser will have 
        // set if the user has checked the Make private checkbox
        if ($_POST["tvpr_make_private"] == "true") {
            // If the user does want to make this file private then
            // hook the upload_dir filter so that we can redirect the file into our private directory
            add_filter("upload_dir", function (array $dirs) {
                // insert /private in front of the subdir if it hasn't already been inserted
                $originalSubDir = $dirs["subdir"];

                if (strpos($originalSubDir, "/private") === false) {
                    $newSubDir = "/private" . $originalSubDir;

                    // update each of the paths to use the new subdir
                    $dirs["path"] = str_replace($originalSubDir, $newSubDir, $dirs["path"]);
                    $dirs["url"] = str_replace($originalSubDir, $newSubDir, $dirs["url"]);

                    // update the subdir
                    $dirs["subdir"] = $newSubDir;
                }

                return $dirs;
            });
        }

        return $file;
    }

    public function update_metadata_for_private_attachement($postId)
    {
        // this action is called when an attachement has been added for the first time
        // and the user has chosen to make it private
        if ($_POST["tvpr_make_private"] == "true") {
            update_post_meta($postId, 'tv_churches_restricted_roles', array());
        }
    }
}
