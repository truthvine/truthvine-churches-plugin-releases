<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.truthvine.com
 * @since      1.0.0
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Truthvine_Churches
 * @subpackage Truthvine_Churches/admin
 * @author     TruthVine CIC <team@truthvine.com>
 */
class Truthvine_Churches_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/truthvine-churches-admin.css', array(), $this->version, 'all' );

	}

	public function register_scripts() {
		$adminBundle = include TRUTHVINE_CHURCHES_DIR.'/dist/admin.bundle.asset.php';
		wp_register_script(
			'truthvine-churches-admin',
			plugin_dir_url( __FILE__ ) . '../dist/admin.bundle.js',
			$adminBundle['dependencies'],
			$adminBundle['version']
		);

		$blockEditorBundle = include TRUTHVINE_CHURCHES_DIR.'/dist/block-editor.bundle.asset.php';
		wp_register_script(
			'truthvine-churches-block-editor',
			plugin_dir_url( __FILE__ ) . '../dist/block-editor.bundle.js',
			$blockEditorBundle['dependencies'], 
			$blockEditorBundle['version']
		);
	}
	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script('truthvine-churches-admin');
	}

	public function enqueue_block_editor_scripts() {
		wp_enqueue_script('truthvine-churches-block-editor');
	}

}
